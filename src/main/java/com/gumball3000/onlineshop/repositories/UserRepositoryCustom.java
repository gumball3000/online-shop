package com.gumball3000.onlineshop.repositories;

import com.gumball3000.onlineshop.domain.User;

public interface UserRepositoryCustom {
    void refresh(User refresh);
}
