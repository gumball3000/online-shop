package com.gumball3000.onlineshop.repositories;

import com.gumball3000.onlineshop.domain.BasketProduct;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface BasketProductRepository extends CrudRepository<BasketProduct, Long> {
}
