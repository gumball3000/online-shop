package com.gumball3000.onlineshop.repositories;

import com.gumball3000.onlineshop.domain.User;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepository extends CrudRepository<User, Long>, UserRepositoryCustom {
    User findByUsernameIgnoreCase(String username);
}
