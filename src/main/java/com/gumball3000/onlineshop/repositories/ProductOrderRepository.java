package com.gumball3000.onlineshop.repositories;

import com.gumball3000.onlineshop.domain.ProductOrder;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ProductOrderRepository extends CrudRepository<ProductOrder, Long> {
}
