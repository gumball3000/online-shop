package com.gumball3000.onlineshop.repositories;

import com.gumball3000.onlineshop.domain.User;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;

public class UserRepositoryImpl implements UserRepositoryCustom {
    @PersistenceContext
    private EntityManager em;
    @Override
    @Transactional
    public void refresh(User user) {
        em.refresh(user);
    }
}
