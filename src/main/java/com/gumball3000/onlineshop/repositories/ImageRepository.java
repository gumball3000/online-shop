package com.gumball3000.onlineshop.repositories;

import com.gumball3000.onlineshop.domain.Image;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ImageRepository extends CrudRepository<Image, Long> {

}
