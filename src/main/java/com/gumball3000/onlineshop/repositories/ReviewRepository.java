package com.gumball3000.onlineshop.repositories;

import com.gumball3000.onlineshop.domain.Review;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ReviewRepository extends CrudRepository<Review, Long> {

}
