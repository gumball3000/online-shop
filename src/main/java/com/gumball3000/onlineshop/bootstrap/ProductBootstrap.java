package com.gumball3000.onlineshop.bootstrap;

import com.gumball3000.onlineshop.repositories.BasketProductRepository;
import com.gumball3000.onlineshop.repositories.ProductOrderRepository;
import com.gumball3000.onlineshop.repositories.ProductRepository;
import com.gumball3000.onlineshop.repositories.UserRepository;
import com.gumball3000.onlineshop.services.ProductService;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

@Component
public class ProductBootstrap implements ApplicationListener<ContextRefreshedEvent> {
    private final ProductRepository productRepository;
    private final ProductService productService;
    private final UserRepository userRepository;
    private final PasswordEncoder passwordEncoder;
    private final BasketProductRepository basketProductRepository;
    private final ProductOrderRepository productOrderRepository;

    public ProductBootstrap(ProductRepository productRepository, ProductService productService, UserRepository userRepository, PasswordEncoder passwordEncoder, BasketProductRepository basketProductRepository, ProductOrderRepository productOrderRepository) {
        this.productRepository = productRepository;
        this.productService = productService;
        this.userRepository = userRepository;
        this.passwordEncoder = passwordEncoder;
        this.basketProductRepository = basketProductRepository;
        this.productOrderRepository = productOrderRepository;
    }


    @Override
    @Transactional
    public void onApplicationEvent(ContextRefreshedEvent contextRefreshedEvent) {
        //loadData();
    }

    private void loadData(){
/*        Product product1 = new Product(1L,"telefon jmecher", 2.0, 2,2, "android",
                "company description", "description", 99.99, "Samsung", "Phones");
        productRepository.save(product1);
        Product product2 = new Product(2L,"telefon si mai jmecher", 2.0, 2,2, "android",
                "company description", "description", 99.99, "HTC", "Phones");
        productRepository.save(product2);
        product2 = new Product(3L,"telefon si mai jmecher", 2.0, 2,2, "android",
                "company description", "description", 99.99, "Samsung", "Phones");
        productRepository.save(product2);
        product2 = new Product(4L,"telefon si mai jmecher", 2.0, 2,2, "android",
                "company description", "description", 99.99, "Apple", "Phones");
        productRepository.save(product2);
        product2 = new Product(5L,"telefon si mai jmecher", 2.0, 2,2, "android",
                "company description", "description", 99.99, "Samsung", "Phones");
        productRepository.save(product2);
        product2 = new Product(6L,"telefon si mai jmecher", 2.0, 2,2, "android",
                "company description", "description", 99.99, "Samsung", "Tablets");
        productRepository.save(product2);
        product2 = new Product(7L,"telefon si mai jmecher", 2.0, 2,2, "android",
                "company description", "description", 99.99, "Samsung", "Phones");
        productRepository.save(product2);
        product2 = new Product(8L,"telefon si mai jmecher", 2.0, 2,2, "android",
                "company description", "description", 99.99, "Samsung", "Phones");
        productRepository.save(product2);
        product2 = new Product(9L,"telefon si mai jmecher", 2.0, 2,2, "android",
                "company description", "description", 99.99, "Samsung", "Phones");
        productRepository.save(product2);
        product2 = new Product(10L,"telefon si mai jmecher", 2.0, 2,2, "android",
                "company description", "description", 99.99, "Samsung", "Phones");
        productRepository.save(product2);
        product2 = new Product(11L,"telefon si mai jmecher", 2.0, 2,2, "android",
                "company description", "description", 99.99, "Samsung", "Phones");
        productRepository.save(product2);
        product2 = new Product(12L,"telefon si mai jmecher", 2.0, 2,2, "android",
                "company description", "description", 99.99, "Samsung", "Phones");
        productRepository.save(product2);
        product2 = new Product(13L,"telefon si mai jmecher", 2.0, 2,2, "android",
                "company description", "description", 99.99, "Samsung", "Phones");
        productRepository.save(product2);
        product2 = new Product(15L,"telefon si mai jmecher", 2.0, 2,2, "android",
                "company description", "description", 99.99, "Samsung", "Phones");
        productRepository.save(product2);
        product2 = new Product(16L,"telefon si mai jmecher", 2.0, 2,2, "android",
                "company description", "description", 99.99, "Samsung", "Phones");
        productRepository.save(product2);
        product2 = new Product(17L,"telefon si mai jmecher", 2.0, 2,2, "android",
                "company description", "description", 99.99, "Samsung", "Phones");
        productRepository.save(product2);
        product2 = new Product(18L,"telefon si mai jmecher", 2.0, 2,2, "android",
                "company description", "description", 99.99, "Samsung", "Phones");
        productRepository.save(product2);
        product2 = new Product(19L,"telefon si mai jmecher", 2.0, 2,2, "android",
                "company description", "description", 99.99, "Samsung", "Phones");
        productRepository.save(product2);
        product2 = new Product(20L,"telefon si mai jmecher", 2.0, 2,2, "android",
                "company description", "description", 99.99, "Samsung", "Phones");
        productRepository.save(product2);*/
//        productRepository.findAll().forEach(product -> {
//            product.setDisabled(false);
//            productRepository.save(product);
//        });

//            productOrderRepository.findAll().forEach(po -> {
//            po.setDate(Utility.getCurrentDate());
//            productOrderRepository.save(po);
//        });

        //Review review = new Review(1L, 4,"Adrian", "very nice phone", Instant.ofEpochMilli(System.currentTimeMillis()).atZone( ZoneId.systemDefault()).toLocalDate());
        //productService.addReviewToProduct( 3L,review );
//        User dan = new User("dan@dan.com",passwordEncoder.encode("dan123"),"USER","");
//        userRepository.save( dan );
    }
}
