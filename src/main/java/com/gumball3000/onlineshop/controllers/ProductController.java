package com.gumball3000.onlineshop.controllers;

import com.gumball3000.onlineshop.commands.OrderCommand;
import com.gumball3000.onlineshop.commands.ReviewCommand;
import com.gumball3000.onlineshop.domain.Image;
import com.gumball3000.onlineshop.domain.Product;
import com.gumball3000.onlineshop.domain.Review;
import com.gumball3000.onlineshop.domain.User;
import com.gumball3000.onlineshop.repositories.UserRepository;
import com.gumball3000.onlineshop.services.ImageService;
import com.gumball3000.onlineshop.services.OrderService;
import com.gumball3000.onlineshop.services.ProductService;
import org.apache.tomcat.util.http.fileupload.IOUtils;
import org.springframework.beans.propertyeditors.StringTrimmerEditor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.security.Principal;
import java.util.Set;
import java.util.TreeSet;

@Controller
@RequestMapping("/products")
public class ProductController {
    private final ProductService    productService;
    private final ImageService      imageService;
    private final OrderService      orderService;
    private final UserRepository    userRepository;

    public ProductController(ProductService productService,
                             ImageService imageService,
                             OrderService orderService,
                             UserRepository userRepository) {
        this.productService = productService;
        this.imageService   = imageService;
        this.orderService   = orderService;
        this.userRepository = userRepository;
    }

    @InitBinder("product")
    public void initBinder(WebDataBinder binder) {
        binder.registerCustomEditor(String.class, new StringTrimmerEditor(true));

    }

    @RequestMapping("/{id}")
    public String showById(@PathVariable String id, Model model){
        Product product = productService.getProductById(new Long(id));
        //OrderCommand orderCommand = new OrderCommand();
        Set<Review> reviews = new TreeSet<>(  );
        product.getReviews().forEach( reviews::add );
        System.out.println(productService.generateStarRatingForProduct(product.getId()));
        model.addAttribute("stars", productService.generateStarRatingForProduct(product.getId()));
        model.addAttribute( "reviews", reviews );
        model.addAttribute( "product", product);
        model.addAttribute( "orderCommand", new OrderCommand());
        model.addAttribute("reviewCommand", new ReviewCommand());
        return "product-page";
    }



    @GetMapping("/image/{imageId}")
    public void renderImageFromDB(@PathVariable String imageId,
                                  HttpServletResponse response) throws IOException {
        Image image = imageService.getImageById(Long.valueOf(imageId));
        if (image.getData() != null) {
            byte[] byteArray = new byte[image.getData().length];
            int i = 0;
            for (Byte wrappedByte : image.getData()){
                byteArray[i++] = wrappedByte; //auto unboxing
            }
            response.setContentType("image/jpeg");
            InputStream is = new ByteArrayInputStream(byteArray);
            IOUtils.copy(is, response.getOutputStream());
        }
    }

    @PostMapping("/{id}/postreview")
    public String postReview(@ModelAttribute @Valid ReviewCommand command,
                              BindingResult bindingResult,
                              @PathVariable String id,
                              Model model,
                             Principal principal,
                             HttpServletRequest request){
        if (request.getUserPrincipal()!=null) {
        User user = userRepository.findByUsernameIgnoreCase(principal.getName());
            if (!bindingResult.hasErrors()) {
                StringBuilder sb = new StringBuilder();
                sb.append(user.getFirstName());
                sb.append(" ");
                sb.append(user.getLastName());
                command.setProductId(new Long(id));
                productService.addReviewToProduct(command, sb.toString());
            }
        }
        return "redirect:/products/" + id;
    }

    @RequestMapping("/addtobasket")
    public String addToBasket(@ModelAttribute OrderCommand command,Model model){
        if (orderService.getCurrentOrderForUserId( command.getUserId()) != null) {
            orderService.addProductToOrder( orderService.getCurrentOrderForUserId( command.getUserId() ).getId(),
                    productService.getProductById( command.getProductId()) );
        } else {
            orderService.addProductToOrder( orderService.createNewOrder( command.getUserId() ).getId(),
                    productService.getProductById( command.getProductId()) );
        }
        return "redirect:/shopping-cart";
    }


}
