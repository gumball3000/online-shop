package com.gumball3000.onlineshop.controllers;

import com.gumball3000.onlineshop.domain.ProductOrder;
import com.gumball3000.onlineshop.domain.User;
import com.gumball3000.onlineshop.repositories.ProductOrderRepository;
import com.gumball3000.onlineshop.repositories.UserRepository;
import com.gumball3000.onlineshop.services.OrderService;
import com.gumball3000.onlineshop.utility.Utility;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;
import java.security.Principal;
import java.util.Set;
import java.util.TreeSet;

@Controller
public class MyOrdersController {
    private final OrderService              orderService;
    private final UserRepository            userRepository;
    private final ProductOrderRepository    productOrderRepository;

    public MyOrdersController(OrderService orderService,
                              UserRepository userRepository,
                              ProductOrderRepository productOrderRepository) {
        this.orderService           = orderService;
        this.userRepository         = userRepository;
        this.productOrderRepository = productOrderRepository;
    }

    @RequestMapping("/my-orders")
    public String getIndexPage(Model model,
                               Principal principal,
                               HttpServletRequest request){
        if (request.getUserPrincipal()!=null) {
            Set<ProductOrder> orders = new TreeSet<>();
            orderService.getOrdersForUserId(
                    userRepository.findByUsernameIgnoreCase(principal.getName()).
                            getId()).forEach(productOrder -> {
                                if (!productOrder.getIsCurrent()){
                                    orders.add(productOrder);
                                }
            });
            model.addAttribute( "orders", orders);
            return "my-orders";
        } else {
            return "redirect:/login";
        }
    }
    @RequestMapping("/my-orders/{orderId}")
    public String viewOrder(Model model,
                            Principal principal,
                            HttpServletRequest request,
                            @PathVariable String orderId){
        User user = userRepository.findByUsernameIgnoreCase(principal.getName());
        ProductOrder order = productOrderRepository.findById(new Long(orderId)).orElse(null);
        if (user.getId() == order.getUser().getId()){
            model.addAttribute("subtotal", Utility.getSubtotal(order));
            model.addAttribute( "orderService", orderService );
            model.addAttribute("products", order.getProducts());
            model.addAttribute( "emptyBasket", false );
            model.addAttribute( "isDetails", true );
            return "shopping-cart";
        } else {
            return "redirect:/";
        }
    }
}
