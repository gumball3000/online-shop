package com.gumball3000.onlineshop.controllers;

import com.gumball3000.onlineshop.commands.CatalogCommand;
import com.gumball3000.onlineshop.domain.Product;
import com.gumball3000.onlineshop.services.ProductService;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.util.Iterator;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

@Controller
public class CatalogController {
    private final ProductService productService;

    public CatalogController(ProductService productService) {
        this.productService = productService;
    }

    @RequestMapping({"/catalog-page"})
    public String getCatalogPage(Model model,
                                 @RequestParam("page") Optional<Integer> page,
                                 @RequestParam("size") Optional<Integer> size,
                                 @RequestParam("category") Optional<String> category,
                                 @RequestParam("brand") Optional<String> brand,
                                 @RequestParam("os") Optional<String> os){
        Set<Product> products = productService.getProducts();
        Iterator<Product> i = products.iterator();
        int currentPage = page.orElse(1);
        int pageSize = size.orElse(9);
        if(category.isPresent()){
            while (i.hasNext()){
                Product product = i.next();
                if (!product.getCategory().equals( category.orElse("All"))){

                    i.remove();
                }
            }
        }

        if(brand.isPresent()){
            while (i.hasNext()){
                Product product = i.next();
                if (!product.getBrand().equals( brand.orElse("All") )){
                    i.remove();
                }
            }
        }
        if(os.isPresent()){
            while (i.hasNext()){
                Product product = i.next();
                if (!product.getOs().equals( os.orElse("All") )){
                    i.remove();
                }
            }
        }
        Page<Product> productPage;
        model.addAttribute("products", products);
        model.addAttribute("productService", productService);
        if(!model.containsAttribute("catalogCommand")){
            //model.addAttribute("products", productService.getProducts());
            CatalogCommand catalogCommand = new CatalogCommand();
            catalogCommand.setBrand(brand.orElse("All"));
            catalogCommand.setCategory(category.orElse("All"));
            catalogCommand.setOs(os.orElse("All"));
            catalogCommand.setBrands( productService.getBrands() );
            catalogCommand.setCategories( productService.getCategories() );
            catalogCommand.setOss( productService.getOss() );
            model.addAttribute( "catalogCommand", catalogCommand);
            productPage = productService.findPaginated(PageRequest.of(currentPage - 1, pageSize), products);
            model.addAttribute("productPage", productPage);
        } else {
            //Set<Product> products = (Set<Product>) model.asMap().get("products");
            productPage = productService.findPaginated(PageRequest.of(currentPage - 1, pageSize), products);
            model.addAttribute("productPage", productPage);
        }
        int totalPages = productPage.getTotalPages();
        if (totalPages > 0) {
            List<Integer> pageNumbers = IntStream.rangeClosed(1, totalPages)
                    .boxed()
                    .collect(Collectors.toList());
            model.addAttribute("pageNumbers", pageNumbers);
        }
        return"catalog-page";

    }

    @PostMapping({"/catalog-page"})
    public String getFilteredCatalogItems(@ModelAttribute CatalogCommand catalogCommand,
                                          BindingResult bindingResult, Model model,
                                         RedirectAttributes redirectAttributes){
        StringBuilder params = new StringBuilder();
        params.append("?");
        if (!catalogCommand.getBrand().equals( "All" )){
            params.append("brand=");
            params.append(catalogCommand.getBrand());
            if (!catalogCommand.getCategory().equals( "All" ) || !catalogCommand.getOs().equals( "All" )){
                params.append("&");
            }
        }

        if (!catalogCommand.getCategory().equals( "All" )){
            params.append("category=");
            params.append(catalogCommand.getCategory());
            if (!catalogCommand.getOs().equals( "All" )) {
                params.append("&");
            }
        }

        if (!catalogCommand.getOs().equals( "All" )){
            params.append("os=");
            params.append(catalogCommand.getOs());
        }
        redirectAttributes.addFlashAttribute("org.springframework.validation.BindingResult.catalogCommand",
                bindingResult);
        catalogCommand = new CatalogCommand();
        catalogCommand.setBrands( productService.getBrands() );
        catalogCommand.setCategories( productService.getCategories() );
        catalogCommand.setOss( productService.getOss() );
        redirectAttributes.addFlashAttribute("catalogCommand", catalogCommand);
        //redirectAttributes.addFlashAttribute("products", products);
        return "redirect:/catalog-page" + params.toString();
    }
}
