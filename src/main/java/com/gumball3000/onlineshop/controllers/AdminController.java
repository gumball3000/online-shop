package com.gumball3000.onlineshop.controllers;

import com.gumball3000.onlineshop.commands.ProductCommand;
import com.gumball3000.onlineshop.domain.Product;
import com.gumball3000.onlineshop.domain.ProductOrder;
import com.gumball3000.onlineshop.repositories.ProductOrderRepository;
import com.gumball3000.onlineshop.repositories.ProductRepository;
import com.gumball3000.onlineshop.services.OrderService;
import com.gumball3000.onlineshop.services.ProductService;
import com.gumball3000.onlineshop.utility.Utility;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import java.security.Principal;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

@Controller
@RequestMapping("/admin")
public class AdminController {
    private final ProductRepository         productRepository;
    private final ProductOrderRepository    productOrderRepository;
    private final OrderService              orderService;
    private final ProductService            productService;

    public AdminController(ProductRepository productRepository,
                           ProductOrderRepository productOrderRepository,
                           OrderService orderService, ProductService productService) {
        this.productRepository      = productRepository;
        this.productOrderRepository = productOrderRepository;
        this.orderService           = orderService;
        this.productService = productService;
    }

    @RequestMapping("/manage-products")
    public String getIndexPage(Model model,
                               Principal principal,
                               HttpServletRequest request){
        if (request.getUserPrincipal()!=null) {
            Set<Product> products = new TreeSet<>();
            productRepository.findAll().forEach(products::add);
            model.addAttribute( "products", products );
            return "manage-products";
        } else {
            return "redirect:/login";
        }
    }

    @RequestMapping("/all-orders")
    public String getAllOrders(Model model,
                               Principal principal,
                               HttpServletRequest request) {
        if (request.getUserPrincipal() != null) {
            Set<ProductOrder> orders = new TreeSet<>();
            productOrderRepository.findAll().forEach(productOrder -> {
                if (!productOrder.getIsCurrent()) {
                    orders.add(productOrder);
                }
            });
            model.addAttribute("orders", orders);
            return "all-orders";
        } else {
            return "redirect:/login";
        }
    }

    @RequestMapping("/all-orders/{orderId}")
    public String getOrder(Model model,
                           Principal principal,
                           HttpServletRequest request,
                           @PathVariable String orderId) {
        ProductOrder order = productOrderRepository.findById(new Long(orderId)).orElse(null);
        if (order != null) {
            model.addAttribute("subtotal", Utility.getSubtotal(order));
            model.addAttribute("orderService", orderService);
            model.addAttribute("products", order.getProducts());
            model.addAttribute("emptyBasket", false);
            model.addAttribute("isDetails", true);
            return "shopping-cart";
        } else {
            return "redirect:/";
        }
    }


    @RequestMapping("/enableProduct/{productId}")
    public String enableProduct(Model model,
                               Principal principal,
                               HttpServletRequest request,
                               @PathVariable String productId){
        if (request.getUserPrincipal()!=null) {
            Product product = productRepository.findById(new Long(productId)).orElse(null);
            if (product != null){
                product.setDisabled(false);
                productRepository.save(product);
            }
            return "redirect:/admin/manage-products";
        } else {
            return "redirect:/login";
        }
    }

    @RequestMapping("/disableProduct/{productId}")
    public String disableProduct(Model model,
                               Principal principal,
                               HttpServletRequest request,
                               @PathVariable String productId){
        if (request.getUserPrincipal() != null) {
            Product product = productRepository.findById(new Long(productId)).orElse(null);
            if (product != null){
                product.setDisabled(true);
                productRepository.save(product);
            }
            return "redirect:/admin/manage-products";
        } else {
            return "redirect:/login";
        }
    }

    @RequestMapping("/add-product")
    public String showAddProductPage(Model model, HttpSession session){
        session.setAttribute("emptyFields", false);
        if (!model.containsAttribute("productCommand")) {
            model.addAttribute("productCommand", new ProductCommand());
        }
        return "add-product";
    }

    @PostMapping("/add-product")
    public String addProduct(@ModelAttribute @Valid ProductCommand command, BindingResult bindingResult, Model model,
                             RedirectAttributes redirectAttributes, HttpSession session){
        if (bindingResult.hasErrors()){
            redirectAttributes.addFlashAttribute("org.springframework.validation.BindingResult.productCommand", bindingResult);
            redirectAttributes.addFlashAttribute("productCommand", command);
            return "redirect:/admin/add-product";
        } else {
            if (checkFiles(command.getFile(), command.getFiles())) {
                model.addAttribute("productCommand", new ProductCommand());
                ProductCommand productCommand = productService.addNewProduct(command);
                return "redirect:/products/" + productCommand.getId();
            } else {
                command.setFile(null);
                command.setFiles(new ArrayList<>());
                redirectAttributes.addFlashAttribute("productCommand", command);
                return "redirect:/products/add?invalidFiles=true";
            }
        }
    }

    public boolean checkFiles(MultipartFile file, List<MultipartFile> files){
        if (!Utility.isValidImage(file)) return false;
        for (MultipartFile  multipartFile:files) {
            if(!Utility.isValidImage(multipartFile)) return false;
        }
        return true;
    }
}
