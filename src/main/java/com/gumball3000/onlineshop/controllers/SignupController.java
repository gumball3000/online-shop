package com.gumball3000.onlineshop.controllers;

import com.gumball3000.onlineshop.commands.UserCommand;
import com.gumball3000.onlineshop.services.UserService;
import com.gumball3000.onlineshop.utility.Utility;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpSession;

@Controller
public class SignupController {
    private final UserService userService;

    public SignupController(UserService userService) {
        this.userService = userService;
    }

    @RequestMapping("signup")
    public String getIndexPage(Model model, HttpSession session){
        Boolean bool = false;
        session.setAttribute("emptyFields", bool);
        session.setAttribute("emptyFields", bool);
        session.setAttribute("passwordsDontMatch", bool);
        session.setAttribute("usernameHasSpecalCharacters", bool);
        session.setAttribute("usernameTooShort", bool);
        session.setAttribute("passwordTooShort", bool);
        session.setAttribute("nameHasSpecialCharacters", bool);
        session.setAttribute("userExists", bool);
        model.addAttribute( "user", new UserCommand() );
        return"registration";
    }

    @RequestMapping("signupfailed")
    public String getIndexPage2(Model model){
        model.addAttribute( "user", new UserCommand() );
        return"registration";
    }

    @PostMapping("signup/newUser")
    public String addNewUser(@ModelAttribute UserCommand command, Model model, HttpSession session){
        Boolean bool = false;
        session.setAttribute("emptyFields", bool);
        session.setAttribute("emptyFields", bool);
        session.setAttribute("passwordsDontMatch", bool);
        session.setAttribute("usernameHasSpecalCharacters", bool);
        session.setAttribute("usernameTooShort", bool);
        session.setAttribute("passwordTooShort", bool);
        session.setAttribute("nameHasSpecialCharacters", bool);
        session.setAttribute("userExists", bool);
        if (command.getFirstName().trim().isEmpty()  || command.getLastName().trim().isEmpty()
        || command.getUsername().trim().isEmpty() || command.getAddress().trim().isEmpty() ||
        command.getPassword().trim().isEmpty() || command.getRepeatPassword().trim().isEmpty()){
             bool = true;
            session.setAttribute("emptyFields", bool);
            return "redirect:/signupfailed";
        }
        if (userService.userExists(command.getUsername())){
            bool = true;
            session.setAttribute("userExists", bool);
            return "redirect:/signupfailed";
        }
        if (!command.getPassword().equals(command.getRepeatPassword())){
             bool = true;
            session.setAttribute("passwordsDontMatch", bool);
            return "redirect:/signupfailed";
        }

        if (Utility.hasSpecialCharacter(command.getUsername())){
             bool = true;
            session.setAttribute("usernameHasSpecalCharacters", bool);
            return "redirect:/signupfailed";
        }

        if (command.getUsername().trim().length()<4){
             bool = true;
            session.setAttribute("usernameTooShort", bool);
            return "redirect:/signupfailed";
        }

        if (command.getPassword().trim().length()<9){
             bool = true;
            session.setAttribute("passwordTooShort", bool);
            return "redirect:/signupfailed";
        }

        if (Utility.hasMoreThanLetters(command.getFirstName()) || Utility.hasMoreThanLetters(command.getLastName())){
             bool = true;
            session.setAttribute("nameHasSpecialCharacters", bool);
            return "redirect:/signupfailed";
        }
        UserCommand userCommand = userService.addNewUser( command );
        return "redirect:/login?registered=true";
    }
}
