package com.gumball3000.onlineshop.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class LoginController {

    @RequestMapping({"login.html", "login"})
    public String getIndexPage(Model model){
        return"login";
    }
}
