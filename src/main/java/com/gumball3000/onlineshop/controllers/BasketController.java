package com.gumball3000.onlineshop.controllers;

import com.gumball3000.onlineshop.domain.BasketProduct;
import com.gumball3000.onlineshop.domain.ProductOrder;
import com.gumball3000.onlineshop.domain.User;
import com.gumball3000.onlineshop.repositories.BasketProductRepository;
import com.gumball3000.onlineshop.repositories.ProductOrderRepository;
import com.gumball3000.onlineshop.repositories.UserRepository;
import com.gumball3000.onlineshop.services.BasketProductService;
import com.gumball3000.onlineshop.services.OrderService;
import com.gumball3000.onlineshop.services.ProductService;
import com.gumball3000.onlineshop.services.UserService;
import com.gumball3000.onlineshop.utility.Utility;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpServletRequest;
import java.security.Principal;
import java.util.HashSet;
import java.util.Set;

@Controller
@RequestMapping("shopping-cart")
public class BasketController {
    private final UserService               userService;
    private final UserRepository            userRepository;
    private final OrderService              orderService;
    private final BasketProductRepository   basketProductRepository;
    private final BasketProductService      basketProductService;
    private final ProductService            productService;
    private final ProductOrderRepository    productOrderRepository;

    public BasketController(UserService userService,
                            UserRepository userRepository,
                            OrderService orderService,
                            BasketProductRepository basketProductRepository,
                            BasketProductService basketProductService,
                            ProductService productService,
                            ProductOrderRepository productOrderRepository) {
        this.userService                = userService;
        this.userRepository             = userRepository;
        this.orderService               = orderService;
        this.basketProductRepository    = basketProductRepository;
        this.basketProductService       = basketProductService;
        this.productService             = productService;
        this.productOrderRepository     = productOrderRepository;
    }

    @RequestMapping("")
    public String getIndexPage(Model model,
                               HttpServletRequest request,
                               Principal principal){
        if (request.getUserPrincipal()!=null) {
            User user = userRepository.findByUsernameIgnoreCase(principal.getName());
            if (orderService.getCurrentOrderForUserId(user.getId()) != null) {
                Set<BasketProduct> products = userService.getBasketProducts(user.getId());
                model.addAttribute("subtotal", Utility.getSubtotal(orderService.getCurrentOrderForUserId(user.getId())));
                model.addAttribute( "orderService", orderService );
                model.addAttribute("products", products);
                if (products.isEmpty()){
                    model.addAttribute( "emptyBasket", true );
                } else {
                    model.addAttribute( "emptyBasket", false );
                }
                return "shopping-cart";
            } else {
                ProductOrder order = orderService.createNewOrder( user.getId());
                model.addAttribute("products", new HashSet<>(  ) );
                model.addAttribute("subtotal", Utility.getSubtotal(order));
                model.addAttribute( "orderService", orderService );
                model.addAttribute( "emptyBasket", true );
                return "shopping-cart";
            }
        } else {
            return "redirect:/login";
        }
    }

    @PostMapping("/increase/{productId}/{basketProductId}")
    public String increaseProductCount(@PathVariable String productId,
                                       @PathVariable String basketProductId,
                                       Model model, HttpServletRequest request,
                                       Principal principal,
                                       RedirectAttributes redirectAttributes){
        if (request.getUserPrincipal()!=null) {
            BasketProduct bp = basketProductService.getBasketProductById(new Long(basketProductId));
            int amount = bp.getCount();
            if (amount >= 10){
                amount = 10;
            } else {
                amount++;
            }
            if (bp==null){
                System.out.println("BLYAT!");
            } else {
                redirectAttributes.addFlashAttribute("products", orderService.updateProductCount(
                        orderService.getCurrentOrderForUserId(
                                userRepository.findByUsernameIgnoreCase(principal.getName()).
                                getId()).getId(),
                        productService.getProductById(bp.getProduct().getId()),
                        amount).getProducts());
            }
            redirectAttributes.addFlashAttribute("subtotal", Utility.getSubtotal(orderService.getCurrentOrderForUserId(
                    userRepository.findByUsernameIgnoreCase(principal.getName()).getId())));
            return "redirect:/shopping-cart";
        } else {
            return "redirect:/login";
        }
    }

    @PostMapping("/decrease/{productId}/{basketProductId}")
    public String decreaseProductCount(@PathVariable String productId,
                                       @PathVariable String basketProductId,
                                       Model model, HttpServletRequest request,
                                       Principal principal,
                                       RedirectAttributes redirectAttributes){
        if (request.getUserPrincipal()!=null) {
            BasketProduct bp = basketProductService.getBasketProductById(new Long(basketProductId));
            int amount = bp.getCount();
            if (amount <= 1){
                amount = 1;
            } else {
                amount--;
            }
            if (bp==null){
                System.out.println("BLYAT!");
            } else {
                redirectAttributes.addFlashAttribute("products", orderService.updateProductCount(
                        orderService.getCurrentOrderForUserId(
                                userRepository.findByUsernameIgnoreCase(principal.getName()).
                                        getId()).getId(),
                        productService.getProductById(bp.getProduct().getId()),
                        amount).getProducts());
            }
            redirectAttributes.addFlashAttribute("subtotal", Utility.getSubtotal(orderService.getCurrentOrderForUserId(
                    userRepository.findByUsernameIgnoreCase(principal.getName()).getId())));
            return "redirect:/shopping-cart";
        } else {
            return "redirect:/login";
        }
    }

    @PostMapping("/purchase")
    public String purchase(Model model, Principal principal){
        ProductOrder order = orderService.getCurrentOrderForUserId(
                userRepository.findByUsernameIgnoreCase(principal.getName()).
                        getId());
        if (order.getProducts().isEmpty()){
            return "redirect:/shopping-cart";
        } else {
            order.setIsCurrent( false );
            order.setDate(Utility.getCurrentDate());
            productOrderRepository.save( order );
            return "/thank-you";
        }
    }

    @PostMapping("/remove/{basketProductId}")
    public String removeProduct(@PathVariable String basketProductId,
                                Model model, HttpServletRequest request){
        if (request.getUserPrincipal()!=null) {
            BasketProduct bp = basketProductService.getBasketProductById(new Long(basketProductId));
            basketProductRepository.delete(bp);
            return "redirect:/shopping-cart";
        } else {
            return "redirect:/login";
        }
    }
}
