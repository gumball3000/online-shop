package com.gumball3000.onlineshop.utility;

import com.gumball3000.onlineshop.domain.BasketProduct;
import com.gumball3000.onlineshop.domain.ProductOrder;
import org.springframework.web.multipart.MultipartFile;

import java.text.NumberFormat;
import java.time.Instant;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Locale;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Utility {
    public static boolean hasSpecialCharacter(String text){
        Pattern sPattern = Pattern.compile( "[a-zA-Z0-9]*" );
        Matcher sMatcher = sPattern.matcher( text );
        if(!sMatcher.matches()){
            return true;
        } else {
            return false;
        }
    }



    public static boolean hasMoreThanLetters(String text){
        Pattern sPattern = Pattern.compile( "[a-zA-Z]*" );
        Matcher sMatcher = sPattern.matcher( text );
        if(!sMatcher.matches()){
            return true;
        } else {
            return false;
        }
    }

    public static boolean isValidImage(MultipartFile multipartFile) {
        boolean result = true;
        String contentType = multipartFile.getContentType();
        if (multipartFile.isEmpty()){
            result = false;
        }
        if (!isSupportedContentType(contentType)) {
            result = false;
        }
        return result;
    }

    private static boolean isSupportedContentType(String contentType) {
        return contentType.equals("image/png")
                || contentType.equals("image/jpg")
                || contentType.equals("image/jpeg");
    }

    public static String getSubtotal(ProductOrder order){
        Double subtotal = 0.0;
        Set<BasketProduct> products = order.getProducts();
        for (BasketProduct bp:products) {
            subtotal += (Double)(bp.getProduct().getPrice()*bp.getCount());
        }
        Locale locale = new Locale("en", "US");
        NumberFormat fmt = NumberFormat.getCurrencyInstance(locale);
        return fmt.format(new Double(String.format("%.2f", subtotal)));
    }

    public static LocalDate getCurrentDate(){
        return Instant.ofEpochMilli(System.currentTimeMillis()).atZone(ZoneId.systemDefault()).toLocalDate();
    }
}
