package com.gumball3000.onlineshop.converters;

import com.gumball3000.onlineshop.commands.ReviewCommand;
import com.gumball3000.onlineshop.domain.Review;
import lombok.Synchronized;
import org.springframework.core.convert.converter.Converter;
import org.springframework.lang.Nullable;
import org.springframework.stereotype.Component;

@Component
public class ReviewToReviewCommand implements Converter<Review, ReviewCommand> {
    @Synchronized
    @Nullable
    @Override
    public ReviewCommand convert(Review source) {
        if (source == null) {
            return null;
        }
        ReviewCommand reviewCommand = new ReviewCommand();
        reviewCommand.setId(source.getId());
        reviewCommand.setStars(source.getStars());
        reviewCommand.setTitle(source.getTitle());
        reviewCommand.setReview(source.getReview());
        reviewCommand.setDate(source.getDate());
        reviewCommand.setProductId(source.getProduct().getId());
        return reviewCommand;
    }
}
