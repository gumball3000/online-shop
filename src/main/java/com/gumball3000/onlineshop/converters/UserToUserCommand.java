package com.gumball3000.onlineshop.converters;

import com.gumball3000.onlineshop.commands.UserCommand;
import com.gumball3000.onlineshop.domain.User;
import lombok.Synchronized;
import org.springframework.core.convert.converter.Converter;
import org.springframework.lang.Nullable;
import org.springframework.stereotype.Component;

@Component
public class UserToUserCommand implements Converter<User, UserCommand> {
    @Synchronized
    @Nullable
    @Override
    public UserCommand convert(User source) {
        if (source == null){
            return null;
        }
        
        final UserCommand userCommand = new UserCommand();
        userCommand.setId( source.getId() );
        userCommand.setFirstName( source.getFirstName() );
        userCommand.setLastName( source.getLastName() );
        userCommand.setAddress( source.getAddress() );
        userCommand.setUsername( source.getUsername() );
        userCommand.setActive( 1 );
        userCommand.setRoles( source.getRoles() );
        userCommand.setPermissions( source.getPermissions() );
        return userCommand;
    }
}
