package com.gumball3000.onlineshop.converters;

import com.gumball3000.onlineshop.commands.ReviewCommand;
import com.gumball3000.onlineshop.domain.Review;
import com.gumball3000.onlineshop.repositories.ProductRepository;
import lombok.Synchronized;
import org.springframework.core.convert.converter.Converter;
import org.springframework.lang.Nullable;
import org.springframework.stereotype.Component;

import java.time.Instant;
import java.time.ZoneId;
@Component
public class ReviewCommandToReview implements Converter<ReviewCommand, Review> {
    private final ProductRepository productRepository;

    public ReviewCommandToReview(ProductRepository productRepository) {
        this.productRepository = productRepository;
    }

    @Synchronized
    @Nullable
    @Override
    public Review convert(ReviewCommand source) {
        if (source == null) {
            return null;
        }
        Review review = new Review();
//        review.setId(source.getId());
        review.setStars(source.getStars());
        review.setTitle(source.getTitle());
        review.setReview(source.getReview());
        review.setDate(Instant.ofEpochMilli(System.currentTimeMillis()).atZone( ZoneId.systemDefault()).toLocalDate());
        review.setProduct(productRepository.findById(source.getProductId()).orElse(null));
        return review;
    }
}
