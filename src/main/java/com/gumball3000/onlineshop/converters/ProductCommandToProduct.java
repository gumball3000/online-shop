package com.gumball3000.onlineshop.converters;

import com.gumball3000.onlineshop.commands.ProductCommand;
import com.gumball3000.onlineshop.domain.Product;
import lombok.Synchronized;
import org.springframework.core.convert.converter.Converter;
import org.springframework.lang.Nullable;
import org.springframework.stereotype.Component;

@Component
public class ProductCommandToProduct implements Converter<ProductCommand, Product> {
    @Synchronized
    @Nullable
    @Override
    public Product convert(ProductCommand source) {
        if (source == null) {
            return null;
        }
        Product product = new Product();
        product.setId(source.getId());
        product.setName(source.getName());
        product.setBrand(source.getBrand());
        product.setDisplay(source.getDisplay());
        product.setCamera(source.getCamera());
        product.setRam(source.getRam());
        product.setOs(source.getOs());
        product.setCategory( source.getCategory() );
        product.setCompanyDescription(source.getCompanyDescription());
        product.setDescription(source.getDescription());
        product.setPrice(source.getPrice());
        return product;
    }
}
