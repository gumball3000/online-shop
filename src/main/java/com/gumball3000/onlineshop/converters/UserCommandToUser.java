package com.gumball3000.onlineshop.converters;

import com.gumball3000.onlineshop.commands.UserCommand;
import com.gumball3000.onlineshop.domain.User;
import lombok.Synchronized;
import org.springframework.core.convert.converter.Converter;
import org.springframework.lang.Nullable;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

@Component
public class UserCommandToUser implements Converter<UserCommand, User> {
    private final PasswordEncoder passwordEncoder;

    public UserCommandToUser(PasswordEncoder passwordEncoder) {
        this.passwordEncoder = passwordEncoder;
    }

    @Synchronized
    @Nullable
    @Override
    public User convert(UserCommand source) {
        if (source == null) {
            return null;
        }
        final User user = new User();
        user.setId( source.getId() );
        user.setFirstName( source.getFirstName().trim() );
        user.setLastName( source.getLastName().trim() );
        user.setAddress( source.getAddress().trim() );
        user.setUsername( source.getUsername().trim() );
        user.setPassword(passwordEncoder.encode(source.getPassword()));
        user.setActive( 1 );
        if (source.getAdmin() == 0) {
            user.setRoles("USER");
        } else {
            user.setRoles("ADMIN");
        }
        user.setPermissions( "" );
        return user;
    }
}
