package com.gumball3000.onlineshop.converters;

import com.gumball3000.onlineshop.commands.ProductCommand;
import com.gumball3000.onlineshop.domain.Product;
import lombok.Synchronized;
import org.springframework.core.convert.converter.Converter;
import org.springframework.lang.Nullable;
import org.springframework.stereotype.Component;

@Component
public class ProductToProductCommand implements Converter<Product, ProductCommand> {
    @Synchronized
    @Nullable
    @Override
    public ProductCommand convert(Product source) {
        if (source == null) {
            return null;
        }
        ProductCommand productCommand = new ProductCommand();
        productCommand.setId(source.getId());
        productCommand.setName(source.getName());
        productCommand.setBrand(source.getBrand());
        productCommand.setDisplay(source.getDisplay());
        productCommand.setCamera(source.getCamera());
        productCommand.setRam(source.getRam());
        productCommand.setOs(source.getOs());
        productCommand.setCategory( source.getCategory() );
        productCommand.setCompanyDescription(source.getCompanyDescription());
        productCommand.setDescription(source.getDescription());
        productCommand.setPrice(source.getPrice());
        return productCommand;
    }
}
