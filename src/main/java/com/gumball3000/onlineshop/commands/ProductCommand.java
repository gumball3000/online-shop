package com.gumball3000.onlineshop.commands;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.constraints.Positive;
import javax.validation.constraints.Size;
import java.util.List;

@Getter
@Setter
@NoArgsConstructor
public class ProductCommand {
    private Long id;
    @Size(min = 3, max = 24, message="Must be 3-24 characters long")
    private String name;
    @Size(min = 3, max = 24, message="Must be 3-24 characters long")
    private String brand;
    @Positive(message = "Must be a positive value")
    private Double display;
    @Positive(message = "Must be a positive value")
    private Integer camera;
    @Positive(message = "Must be a positive value")
    private Integer ram;
    @Size(min = 3, max = 12, message="Must be 3-12 characters long")
    private String os;
    @Size(min = 3, max = 12, message="Must be 3-12 characters long")
    private String category;
    @Size(min = 3, max = 1000, message="Must be 3-1000 characters long")
    private String companyDescription;
    @Size(min = 3, max = 1000, message="Must be 3-1000 characters long")
    private String description;
    @Positive(message = "Must be a positive value")
    private Double price;
    private MultipartFile file;
    private List<MultipartFile> files;


}
