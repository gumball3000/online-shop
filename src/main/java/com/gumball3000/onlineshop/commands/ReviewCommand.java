package com.gumball3000.onlineshop.commands;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.Size;
import java.time.LocalDate;

@Getter
@Setter
@NoArgsConstructor
public class ReviewCommand {
    private Long id;
    private Integer stars;
    @Size(min = 3, max = 16, message="Must be 3-16 characters long")
    private String title;
    @Size(min = 10, max = 140, message="Must be 10-140 characters long")
    private String review;
    private LocalDate date;
    private Long productId;
}
