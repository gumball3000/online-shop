package com.gumball3000.onlineshop.commands;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class UserCommand {
    private Long id;
    private String firstName;
    private String lastName;
    private String address;
    private String username;
    private String password;
    private String repeatPassword;
    private int active;
    private String roles;
    private String permissions;
    private Integer admin;
    private Boolean emptyFields;
}
