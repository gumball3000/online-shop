package com.gumball3000.onlineshop.domain;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Set;

@Getter
@Setter
@NoArgsConstructor
public class Category {
    private Long id;
    private String name;
    private Set<Product> products;
}
