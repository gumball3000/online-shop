package com.gumball3000.onlineshop.domain;

import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

@Entity
@Data
@EqualsAndHashCode(exclude = {"product"})
public class Review implements Comparable<Review>{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private Integer stars;
    private String title;
    private String review;
    private String name;
    private LocalDate date;
    @ManyToOne
    private Product product;


    public Review() {
    }

    public Review(Long id, int stars, String title, String review, LocalDate date) {
        this.id = id;
        this.stars = stars;
        this.title = title;
        this.review = review;
        this.date = date;
    }

    @Override
    public int compareTo(Review o) {
        return this.id.compareTo( o.getId() );
    }

    public List<Integer> getStarRating() {
        List<Integer> starList = new ArrayList<>();
        for (int i = 1; i <= stars; i++) {
            starList.add(2);
        }
        if (starList.size() != 5) {
            for (int i = starList.size() + 1; i <= 5; i++) {
                starList.add(0);
            }
        }
        return starList;

    }

}
