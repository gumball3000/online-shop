package com.gumball3000.onlineshop.domain;


import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.*;

@Entity
@Data
@EqualsAndHashCode(exclude = {"product"})
public class Image {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Lob
    private Byte[] data;
    @ManyToOne
    private Product product;
    private Boolean isThumbnail;

    public Image() {
    }

    public Image(Byte[] data, Boolean isThumbnail) {
        this.data = data;
        this.product = product;
        this.isThumbnail = isThumbnail;
    }
}
