package com.gumball3000.onlineshop.domain;

import lombok.Data;
import lombok.EqualsAndHashCode;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.Objects;
import java.util.Set;
import java.util.TreeSet;

@Data
@Entity
@EqualsAndHashCode(exclude = {"user"})
public class ProductOrder implements Comparable<ProductOrder>{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @OneToMany(cascade = CascadeType.MERGE, mappedBy = "productOrder", fetch = FetchType.EAGER)
    @Fetch( FetchMode.JOIN )
    @OrderBy("id ASC")
    private Set<BasketProduct> products = new TreeSet<>(  );
    @ManyToOne
    private User user;
    private LocalDate date;
    private Boolean isCurrent;

    public ProductOrder() {
    }

    public ProductOrder(User user, Boolean isCurrent) {
        this.user = user;
        this.isCurrent = isCurrent;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ProductOrder that = (ProductOrder) o;
        return Objects.equals(id, that.id) &&
                Objects.equals(isCurrent, that.isCurrent);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, isCurrent);
    }

    @Override
    public int compareTo(ProductOrder o) {
        return this.getId().compareTo( o.getId() );
    }
}
