package com.gumball3000.onlineshop.domain;


import lombok.Data;

import javax.persistence.*;
import java.text.NumberFormat;
import java.util.HashSet;
import java.util.Locale;
import java.util.Set;
import java.util.TreeSet;

@Data
@Entity
public class Product implements Comparable<Product>{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String brand;
    private String name;
    private Double display;
    private Integer camera;
    private Integer ram;
    private String os;
    @Column(columnDefinition="text")
    private String companyDescription;
    @Column(columnDefinition="text")
    private String description;
    private Double price;
    private String category;
    private Boolean disabled;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "product")
    private Set<Review> reviews = new TreeSet<>();
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "product")
    private Set<Image> images = new HashSet<>();

    public Product() {
    }

    public Product(Long id, String name, Double display, Integer camera, Integer ram, String os,
                   String companyDescription, String description, Double price, String brand, String category) {
        this.id = id;
        this.name = name;
        this.display = display;
        this.camera = camera;
        this.ram = ram;
        this.os = os;
        this.companyDescription = companyDescription;
        this.description = description;
        this.price = price;
        this.brand = brand;
        this.category = category;
    }

    @Override
    public int compareTo(Product product) {
        return this.id.compareTo(product.getId());
    }

    public String getFormattedPrice(){
        Locale locale = new Locale("en", "US");
        NumberFormat fmt = NumberFormat.getCurrencyInstance(locale);
        return fmt.format(new Double(String.format("%.2f", price)));
    }
}
