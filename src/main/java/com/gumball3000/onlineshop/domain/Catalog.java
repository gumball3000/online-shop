package com.gumball3000.onlineshop.domain;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Set;

@Getter
@Setter
@NoArgsConstructor
public class Catalog {
    public String category;
    public String brand;
    public String os;
    public Set<String> categories;
    public Set<String> brands;
    public Set<String> oss;
}