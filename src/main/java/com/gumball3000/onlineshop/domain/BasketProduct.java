package com.gumball3000.onlineshop.domain;

import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.*;
import java.util.Objects;

@Data
@Entity
@EqualsAndHashCode(exclude = {"productOrder"})
public class BasketProduct implements Comparable<BasketProduct>{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @OneToOne(fetch = FetchType.EAGER)
    private Product product;
    private Integer count;
    private Double price;
    @ManyToOne
    private ProductOrder productOrder;

    public BasketProduct() {
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        BasketProduct that = (BasketProduct) o;
        return Objects.equals(id, that.id) &&
                Objects.equals(count, that.count);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, count);
    }

    @Override
    public int compareTo(BasketProduct basketProduct) {
        return this.getId().compareTo(basketProduct.getId());
    }
}
