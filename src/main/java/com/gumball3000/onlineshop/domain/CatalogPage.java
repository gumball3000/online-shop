package com.gumball3000.onlineshop.domain;

import java.util.List;

public class CatalogPage {
    List<Product> products;

    public CatalogPage() {
    }

    public List<Product> getProducts() {
        return products;
    }

    public void setProducts(List<Product> products) {
        this.products = products;
    }
}
