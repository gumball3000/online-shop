package com.gumball3000.onlineshop.securingweb;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.support.ReloadableResourceBundleMessageSource;

@Configuration
public class Conf{
    @Bean(name = "messageSource")
    public ReloadableResourceBundleMessageSource messageSource() {
        ReloadableResourceBundleMessageSource messageBundle = new ReloadableResourceBundleMessageSource();
        messageBundle.setBasename("messages");
        messageBundle.setDefaultEncoding("UTF-8");
        messageBundle.setCacheSeconds(1);
        return messageBundle;
    }
}
