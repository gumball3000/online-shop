package com.gumball3000.onlineshop.services;

import com.gumball3000.onlineshop.domain.BasketProduct;
import com.gumball3000.onlineshop.domain.Product;
import com.gumball3000.onlineshop.domain.ProductOrder;
import com.gumball3000.onlineshop.domain.User;
import com.gumball3000.onlineshop.repositories.BasketProductRepository;
import com.gumball3000.onlineshop.repositories.ProductOrderRepository;
import com.gumball3000.onlineshop.repositories.UserRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.text.NumberFormat;
import java.util.*;

@Service
public class OrderServiceImpl implements OrderService{
    private final ProductOrderRepository productOrderRepository;
    private final UserService userService;
    private final UserRepository userRepository;
    private final BasketProductRepository basketProductRepository;
    public OrderServiceImpl(ProductOrderRepository productOrderRepository, UserService userService, UserRepository userRepository, BasketProductService basketProductService, BasketProductRepository basketProductRepository) {
        this.productOrderRepository = productOrderRepository;
        this.userService = userService;
        this.userRepository = userRepository;
        this.basketProductRepository = basketProductRepository;
    }

    @Transactional
    public ProductOrder getCurrentOrderForUserId(Long userId){
        User user = userService.findById( userId );
        for (ProductOrder order:user.getOrders()) {
            if (order.getIsCurrent()){
                return order;
            }
        }
        return null;
    }

    @Override
    @Transactional
    public ProductOrder addProductToOrder(Long orderId, Product product) {
        ProductOrder order = productOrderRepository.findById( orderId ).orElse( null );
        if (order!=null){
            Iterator<BasketProduct> ib = order.getProducts().iterator();
            while(ib.hasNext()){
                BasketProduct bp = ib.next();
                if (bp.getProduct().getId() == product.getId()){
                    return null;
                }
            }
            BasketProduct basketProduct = new BasketProduct();
            basketProduct.setProduct( product );
            basketProduct.setProductOrder( order );
            basketProduct.setCount( 1 );
            basketProduct.setPrice( product.getPrice() );
            BasketProduct bp = basketProductRepository.save( basketProduct );

            order.getProducts().add( bp );
            ProductOrder savedOrder = productOrderRepository.save( order );
            User user = userService.findById(savedOrder.getUser().getId());
            Iterator<ProductOrder> i = user.getOrders().iterator();
            while(i.hasNext()){
                ProductOrder o = i.next();
                if (o.getId() == savedOrder.getId()){
                    i.remove();
                }
            }
            user.getOrders().add(savedOrder);
            userRepository.save(user);
            return savedOrder;
        } else {
            return null;
        }
    }

    @Override
    @Transactional
    public ProductOrder updateProductCount(Long orderId, Product product, Integer amount) {
        ProductOrder order = productOrderRepository.findById( orderId ).orElse( null );
        if (order!=null){
            Iterator<BasketProduct> i = order.getProducts().iterator();
            while (i.hasNext()){
                BasketProduct basketProduct = i.next();
                if (basketProduct.getProduct().getId().equals( product.getId())){
                    basketProduct.setCount( amount );
                    basketProduct.setPrice( basketProduct.getProduct().getPrice()*amount );
                    return productOrderRepository.save(order);
                }
            }
            return order;
        } else {
            return null;
        }
    }

    @Override
    @Transactional
    public Set<Product> getProducts(Long orderId) {
        Set<Product> products= new TreeSet<>(  );
        ProductOrder order = productOrderRepository.findById( orderId ).orElse( null );
        if (order != null){
            if (!order.getProducts().isEmpty()) {
                Iterator<BasketProduct> i = order.getProducts().iterator();
                while (i.hasNext()) {
                    BasketProduct basketProduct = i.next();
                    products.add( basketProduct.getProduct() );
                }
            }
        }
        return products;
    }

    @Override
    @Transactional
    public ProductOrder createNewOrder(Long userId) {
        User user = userService.findById( userId );
        ProductOrder order = new ProductOrder( user, true );
        ProductOrder returnedOrder = productOrderRepository.save( order );
        user.getOrders().add(returnedOrder);
        userRepository.save(user);
        return returnedOrder;
    }

    @Transactional
    @Override
    public String getPriceForBasketProduct(BasketProduct basketProduct){
        Double price = basketProduct.getPrice();
        Locale locale = new Locale("en", "US");
        NumberFormat fmt = NumberFormat.getCurrencyInstance(locale);
        return fmt.format(new Double(String.format("%.2f", price)));
    }

    @Override
    public Set<ProductOrder> getOrdersForUserId(Long userId) {
        User user = userRepository.findById( userId ).orElse( null );
        Set<ProductOrder> orders = new TreeSet<>( Collections.reverseOrder() );
        orders.addAll( user.getOrders() );
        return orders;
    }
}
