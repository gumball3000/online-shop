package com.gumball3000.onlineshop.services;

import com.gumball3000.onlineshop.domain.BasketProduct;
import com.gumball3000.onlineshop.repositories.BasketProductRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class BasketProductServiceImpl implements BasketProductService {
    private final BasketProductRepository basketProductRepository;

    public BasketProductServiceImpl(BasketProductRepository basketProductRepository) {
        this.basketProductRepository = basketProductRepository;
    }

    @Override
    @Transactional
    public BasketProduct getBasketProductById(Long id) {
        return basketProductRepository.findById(id).orElse(null);
    }
}
