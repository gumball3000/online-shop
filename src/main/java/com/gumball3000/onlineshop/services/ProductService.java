package com.gumball3000.onlineshop.services;

import com.gumball3000.onlineshop.commands.ProductCommand;
import com.gumball3000.onlineshop.commands.ReviewCommand;
import com.gumball3000.onlineshop.domain.Product;
import com.gumball3000.onlineshop.domain.Review;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.Set;

public interface ProductService {
    Set<Product> getProducts();
    Product getProductById(Long id);
    Review addReviewToProduct(Long id, Review review);
    ProductCommand addNewProduct(ProductCommand productCommand);
    Set<String> getCategories();
    Set<String> getBrands();
    Set<String> getOss();
    Page<Product> findPaginated(Pageable pageable, Set<Product> products);
    ReviewCommand addReviewToProduct(ReviewCommand reviewCommand, String name);
    List<Integer> generateStarRatingForProduct(Long productId);
    List<Integer> generateStarRatingForReview(Long productId);
//    List<Product> getFilteredProductList(String filter1,String filter2, String filter3);
}
