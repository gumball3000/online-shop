package com.gumball3000.onlineshop.services;

import com.gumball3000.onlineshop.commands.UserCommand;
import com.gumball3000.onlineshop.converters.UserCommandToUser;
import com.gumball3000.onlineshop.converters.UserToUserCommand;
import com.gumball3000.onlineshop.domain.BasketProduct;
import com.gumball3000.onlineshop.domain.ProductOrder;
import com.gumball3000.onlineshop.domain.User;
import com.gumball3000.onlineshop.repositories.UserRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Set;
import java.util.TreeSet;

@Service
public class UserServiceImpl implements UserService{
    private final UserRepository userRepository;
    private final UserCommandToUser userCommandToUser;
    private final UserToUserCommand userToUserCommand;

    public UserServiceImpl(UserRepository userRepository, UserCommandToUser userCommandToUser, UserToUserCommand userToUserCommand) {
        this.userRepository = userRepository;
        this.userCommandToUser = userCommandToUser;
        this.userToUserCommand = userToUserCommand;
    }

    @Override
    @Transactional
    public UserCommand addNewUser(UserCommand command) {
        User detachedUser = userCommandToUser.convert(command);
        User savedUser = userRepository.save( detachedUser );
        return userToUserCommand.convert( savedUser );
    }

    @Override
    public boolean userExists(String user) {
        return userRepository.findByUsernameIgnoreCase(user) != null;
    }

    @Override
    public User findById(Long id) {

        return userRepository.findById( id ).orElse( null );
    }
    @Transactional
    public Set<BasketProduct> getBasketProducts(Long userId) {
        User user = findById(userId);
        ProductOrder order = getCurrentOrder( user.getOrders() );
            if (order != null) {
                Set<BasketProduct> products = new TreeSet<>();
                order.getProducts().iterator().forEachRemaining(products::add);
                return products;
            } else {
                return null;
            }
    }

    public ProductOrder getCurrentOrder(Set<ProductOrder> orders){
        for (ProductOrder o:orders) {
            if (o.getIsCurrent()){
                return o;
            }
        }
        return null;
    }
}
