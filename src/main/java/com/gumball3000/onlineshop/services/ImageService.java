package com.gumball3000.onlineshop.services;

import com.gumball3000.onlineshop.domain.Image;

public interface ImageService {
    Image getImageById(Long id);
}
