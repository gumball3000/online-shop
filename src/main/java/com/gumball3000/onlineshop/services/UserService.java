package com.gumball3000.onlineshop.services;


import com.gumball3000.onlineshop.commands.UserCommand;
import com.gumball3000.onlineshop.domain.BasketProduct;
import com.gumball3000.onlineshop.domain.User;

import java.util.Set;

public interface UserService {
    UserCommand addNewUser(UserCommand command);
    User findById(Long id);
    boolean userExists(String user);
    Set<BasketProduct> getBasketProducts(Long userId);
}
