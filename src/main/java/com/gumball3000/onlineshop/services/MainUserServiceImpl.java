package com.gumball3000.onlineshop.services;

import com.gumball3000.onlineshop.domain.MainUser;
import com.gumball3000.onlineshop.domain.User;
import com.gumball3000.onlineshop.repositories.UserRepository;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

@Service
public class MainUserServiceImpl implements MainUserService {
    private final UserRepository userRepository;

    public MainUserServiceImpl(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    public UserDetails loadUserByUsername(String s) throws UsernameNotFoundException {
        User user = this.userRepository.findByUsernameIgnoreCase(s);
        if (user != null) {
            MainUser mainUser = new MainUser( user );
            return mainUser;
        } else {
            throw new UsernameNotFoundException("Username not found men");
        }
    }
}
