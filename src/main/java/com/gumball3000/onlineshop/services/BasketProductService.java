package com.gumball3000.onlineshop.services;

import com.gumball3000.onlineshop.domain.BasketProduct;

public interface BasketProductService {
    BasketProduct getBasketProductById(Long id);
}
