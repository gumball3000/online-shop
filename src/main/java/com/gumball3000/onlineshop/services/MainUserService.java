package com.gumball3000.onlineshop.services;

import org.springframework.security.core.userdetails.UserDetailsService;

public interface MainUserService extends UserDetailsService {
}
