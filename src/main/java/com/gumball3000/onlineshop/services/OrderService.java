package com.gumball3000.onlineshop.services;

import com.gumball3000.onlineshop.domain.BasketProduct;
import com.gumball3000.onlineshop.domain.Product;
import com.gumball3000.onlineshop.domain.ProductOrder;

import java.util.Set;

public interface OrderService {
    ProductOrder createNewOrder(Long userId);
    ProductOrder getCurrentOrderForUserId(Long userId);
    ProductOrder addProductToOrder(Long orderId,Product product);
    ProductOrder updateProductCount(Long orderId, Product product, Integer amount);
    Set<Product> getProducts(Long orderId);
    String getPriceForBasketProduct(BasketProduct basketProduct);
    Set<ProductOrder> getOrdersForUserId(Long userId);
}
