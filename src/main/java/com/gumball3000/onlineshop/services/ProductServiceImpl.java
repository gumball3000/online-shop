package com.gumball3000.onlineshop.services;

import com.gumball3000.onlineshop.commands.ProductCommand;
import com.gumball3000.onlineshop.commands.ReviewCommand;
import com.gumball3000.onlineshop.converters.ProductCommandToProduct;
import com.gumball3000.onlineshop.converters.ProductToProductCommand;
import com.gumball3000.onlineshop.converters.ReviewCommandToReview;
import com.gumball3000.onlineshop.converters.ReviewToReviewCommand;
import com.gumball3000.onlineshop.domain.Image;
import com.gumball3000.onlineshop.domain.Product;
import com.gumball3000.onlineshop.domain.Review;
import com.gumball3000.onlineshop.repositories.ProductRepository;
import com.gumball3000.onlineshop.repositories.ReviewRepository;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.*;

@Service
public class ProductServiceImpl implements ProductService{
    private final ProductRepository productRepository;
    private final ProductCommandToProduct productCommandToProduct;
    private final ProductToProductCommand productToProductCommand;
    private final ReviewCommandToReview reviewCommandToReview;
    private final ReviewToReviewCommand reviewToReviewCommand;
    private final ReviewRepository reviewRepository;

    public ProductServiceImpl(ProductRepository productRepository, ProductCommandToProduct productCommandToProduct, ProductToProductCommand productToProductCommand, ReviewCommandToReview reviewCommandToReview, ReviewToReviewCommand reviewToReviewCommand, ReviewRepository reviewRepository) {
        this.productRepository = productRepository;
        this.productCommandToProduct = productCommandToProduct;
        this.productToProductCommand = productToProductCommand;
        this.reviewCommandToReview = reviewCommandToReview;
        this.reviewToReviewCommand = reviewToReviewCommand;
        this.reviewRepository = reviewRepository;
    }

    @Override
    public Set<Product> getProducts() {
        SortedSet<Product> productSet = new TreeSet<>();
        productRepository.findAll().iterator().forEachRemaining(productSet::add);
        return productSet;
    }

    @Override
    public Product getProductById(Long id) {
        return productRepository.findById(id).orElse(null);
    }

    @Override
    public Review addReviewToProduct(Long productId, Review review) {
        Product p =  getProductById( productId ) ;
        review.setProduct( p );
        p.getReviews().add( review );
        productRepository.save( p );
        return review;
    }

    @Override
    @Transactional
    public ProductCommand addNewProduct(ProductCommand command) {
        Product detachedProduct = productCommandToProduct.convert(command);
        Set<Image> images = new HashSet<>();
        Image image = buildImageObject(command.getFile(),true);
        image.setProduct(detachedProduct);
        images.add(image);
        for (MultipartFile file:command.getFiles()) {
            image = buildImageObject(file,false);
            image.setProduct(detachedProduct);
            images.add(image);
        }
        detachedProduct.setImages(images);
        detachedProduct.setDisabled(false);
        Product savedProduct = productRepository.save( detachedProduct );
        return productToProductCommand.convert( savedProduct );
    }

    public Image buildImageObject(MultipartFile file, boolean makeThumbnail){
        Image image = new Image();
        try {
            Byte[] byteObjects = new Byte[file.getBytes().length];
            int i = 0;
            for (byte b : file.getBytes()){
                byteObjects[i++] = b;
            }
            image = new Image(byteObjects, makeThumbnail);
            return image;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return image;
    }

    @Override
    public Set<String> getCategories() {
        Set<Product> products = getProducts();
        Set<String> categories = new TreeSet<>(  );
        categories.add( "All" );
        for (Product product : products) {
            if (!product.getDisabled()) {
                categories.add(product.getCategory());
            }
        }
        return categories;
    }

    @Override
    public Set<String> getBrands() {
        Set<Product> products = getProducts();
        Set<String> brands = new TreeSet<>(  );
        brands.add( "All" );
        for (Product product : products) {
            if (!product.getDisabled()) {
                brands.add(product.getBrand());
            }
        }
        return brands;
    }

    @Override
    public Set<String> getOss() {
        Set<Product> products = getProducts();
        Set<String> oss = new TreeSet<>(  );
        oss.add( "All" );
        for (Product product : products) {
            if (!product.getDisabled()) {
                oss.add(product.getOs());
            }
        }
        return oss;
    }

    @Override
    public Page<Product> findPaginated(Pageable pageable, Set<Product> productSet) {
        List<Product> products = new ArrayList<>();
        productSet.iterator().forEachRemaining(product -> {
            if (!product.getDisabled()){
                products.add(product);
            }
        });
        int pageSize = pageable.getPageSize();
        int currentPage = pageable.getPageNumber();
        int startItem = currentPage * pageSize;
        List<Product> list;
        if (products.size() < startItem) {
            list = Collections.emptyList();
        } else {
            int toIndex = Math.min(startItem + pageSize, products.size());
            list = products.subList(startItem, toIndex);
        }
        Page<Product> bookPage
                = new PageImpl<Product>(list, PageRequest.of(currentPage, pageSize), products.size());

        return bookPage;
    }

    @Override
    public ReviewCommand addReviewToProduct(ReviewCommand reviewCommand, String name) {
        Review detachedReview = reviewCommandToReview.convert(reviewCommand);
        Review review = reviewRepository.save(detachedReview);
        review.setName(name);
        Product product = productRepository.findById(review.getProduct().getId()).orElse(null);
        product.getReviews().add(review);
        productRepository.save(product);
        return reviewToReviewCommand.convert(review);
    }

    @Override
    public List<Integer> generateStarRatingForProduct(Long productId) {
        List<Integer> starList = new ArrayList<>();
        Double average = 0.0;
        Double sum = 0.0;
        Product product = productRepository.findById(productId).orElse(null);
        if (product != null) {
            if (!product.getReviews().isEmpty()) {
                for (Review review : product.getReviews()) {
                    sum += review.getStars();
                }
            } else {
                for (int i = 1; i <= 5; i++) {
                    starList.add(0);
                }
                return starList;
            }
            if (sum > 0.0){
                average = sum/product.getReviews().size();
            }
        }

        if (average>0.0){
            for (int i = 1; i <= average; i++) {
                starList.add(2);
            }
            if (starList.size()<5) {
            average -= starList.size();
            if ( average >= 0.5){
                starList.add(1);
            } else {
                starList.add(0);
            }
                for (int i = starList.size() + 1; i <= 5; i++) {
                    starList.add(0);
                }
            }
        }
        return starList;
    }

    @Override
    public List<Integer> generateStarRatingForReview(Long productId) {
        return null;
    }

    //    @Override
//    public List<Product> getFilteredProductList(String filter1, String filter2, String filter3) {
//        return productRepository.findAll(where(tableFilteredBy( "category", filter1 )).and(tableFilteredBy( "brand",  )));
//    }
}
